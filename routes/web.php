<?php

use App\Http\Controllers\Controller;
use App\Http\Livewire\Admin\Index as AdminIndex;
use App\Http\Livewire\Dpk\Index as DPKIndex;
use App\Http\Livewire\Bem\Index as BEMIndex;
use App\Http\Livewire\Dpk\Data as DPKData;
use App\Http\Livewire\Dpk\Tambah as DPKTambah;
use App\Http\Livewire\Dpk\Lihat as DPKLihat;
use App\Http\Livewire\Dpk\Ubah as DPKUbah;
use App\Http\Livewire\Bem\Data as BEMData;
use App\Http\Livewire\Bem\Tambah as BEMTambah;
use App\Http\Livewire\Bem\Lihat as BEMLihat;
use App\Http\Livewire\Bem\Ubah as BEMUbah;
use App\Http\Livewire\Prodi\Data as ProdiData;
use App\Http\Livewire\Prodi\Tambah as ProdiTambah;
use App\Http\Livewire\Prodi\Ubah as ProdiUbah;
use App\Http\Livewire\Kementrian\Data as KementrianData;
use App\Http\Livewire\Kementrian\Tambah as KementrianTambah;
use App\Http\Livewire\Kementrian\Ubah as KementrianUbah;
use App\Http\Livewire\Proposal\Data as ProposalData;
use App\Http\Livewire\Proposal\Tambah as ProposalTambah;
use App\Http\Livewire\Proposal\Revisi as ProposalRevisi;
use App\Http\Livewire\Proposal\Lihat as ProposalLihat;
use App\Http\Livewire\Proposal\Ubah as ProposalUbah;
use App\Http\Livewire\Lpj\Data as LPJData;
use App\Http\Livewire\Lpj\Tambah as LPJTambah;
use App\Http\Livewire\Lpj\Revisi as LPJRevisi;
use App\Http\Livewire\Lpj\Lihat as LPJLihat;
use App\Http\Livewire\Lpj\Ubah as LPJUbah;
use App\Http\Livewire\Periode\Data as PeriodeData;
use App\Http\Livewire\Periode\Tambah as PeriodeTambah;
use App\Http\Livewire\Periode\Ubah as PeriodeUbah;
use App\Http\Livewire\Periode\Lihat as PeriodeLihat;
use App\Http\Livewire\Review\Data as ReviewData;
use App\Http\Livewire\Review\Tambah as ReviewTambah;
use App\Http\Livewire\Review\Ubah as ReviewUbah;
use App\Http\Livewire\Login;
use App\Http\Livewire\Profil;
use App\Http\Livewire\Password;
use App\Http\Livewire\Logout;
use App\Models\DetailLPJ;
use App\Models\DetailProposal;
use App\Models\LPJ;
use App\Models\Proposal;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/coba', function () {
    $proposal = Proposal::leftJoin('lpj', 'proposal.id', '=', 'lpj.proposal_id')->where('lpj.proposal_id', null)->get();
    dd($proposal);
});

Route::middleware('guest')->group(function () {
    Route::get('/', Login::class)->name('login');
});

Route::post('/logout', Controller::class)->middleware('auth');

Route::middleware('auth')->group(function () {
    Route::get('/admin', AdminIndex::class);
    Route::get('/dpk/data', DPKData::class);
    Route::get('/dpk/tambah', DPKTambah::class);
    Route::get('/dpk/lihat/{username}', DPKLihat::class);
    Route::get('/dpk/ubah/{username}', DPKUbah::class);
    Route::get('/bem/data', BEMData::class);
    Route::get('/bem/tambah', BEMTambah::class);
    Route::get('/bem/lihat/{username}', BEMLihat::class);
    Route::get('/bem/ubah/{username}', BEMUbah::class);
    Route::get('/prodi/data', ProdiData::class);
    Route::get('/prodi/tambah', ProdiTambah::class);
    Route::get('/prodi/ubah/{id}', ProdiUbah::class);
    Route::get('/kementrian/data', KementrianData::class);
    Route::get('/kementrian/tambah', KementrianTambah::class);
    Route::get('/kementrian/ubah/{id}', KementrianUbah::class);
    Route::get('/proposal/data', ProposalData::class);
    Route::get('/proposal/tambah', ProposalTambah::class);
    Route::get('/proposal/ubah/{id}', ProposalUbah::class);
    Route::get('/proposal/unduh/{hash}', function ($hash) {
        $proposal = DetailProposal::where('hash', $hash)->first();
        $thn = explode("/", $proposal->proposal->periode->tahun);
        $tahun = $thn[0] . "-" . $thn[1];
        $ex = File::extension($proposal->file);
        return response()->download(storage_path("app/public/$proposal->file"),  'Proposal ' . $proposal->proposal->nama . ' - ' . $tahun . '.' . $ex);
    });
    Route::get('/proposal/revisi/{id}', ProposalRevisi::class);
    Route::get('/proposal/detail/{id}', ProposalLihat::class);
    Route::get('/lpj/data', LPJData::class);
    Route::get('/lpj/tambah/{proposal_id}', LPJTambah::class);
    Route::get('/lpj/ubah/{id}', LPJUbah::class);
    Route::get('/lpj/unduh/{id}', function ($hash) {
        $proposal = DetailLPJ::where('hash', $hash)->first();
        $thn = explode("/", $proposal->lpj->periode->tahun);
        $tahun = $thn[0] . "-" . $thn[1];
        $ex = File::extension($proposal->file);
        return response()->download(storage_path("app/public/$proposal->file"),  'LPJ ' . $proposal->lpj->proposal->nama . ' - ' . $tahun . '.' . $ex);
    });
    Route::get('/lpj/revisi/{id}', LPJRevisi::class);
    Route::get('/lpj/detail/{id}', LPJLihat::class);
    Route::get('/periode/data', PeriodeData::class);
    Route::get('/periode/tambah', PeriodeTambah::class);
    Route::get('/periode/ubah/{id}', PeriodeUbah::class);
    Route::get('/periode/lihat/{id}', PeriodeLihat::class);
    Route::get('/review/data/{lpj_id}', ReviewData::class);
    Route::get('/review/tambah/{lpj_id}', ReviewTambah::class);
    Route::get('/review/ubah/{lpj_id}', ReviewUbah::class);
    Route::get('/profil', Profil::class);
    Route::get('/ubah/password', Password::class);
    Route::get('/log', Logout::class);
});


Route::get('/dpk', DPKIndex::class)->middleware('auth');

Route::get('/bem', BEMIndex::class)->middleware('auth');
