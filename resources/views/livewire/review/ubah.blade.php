<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Review Kegiatan {{$nama}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row d-flex align-items-center">
                                            <label class="col-sm-3 col-form-label">Rating</label>
                                            <div class="col-sm-9">
                                                <div class="rating" wire:ignore></div>
                                                <div>
                                                    @error('rating')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row d-flex align-items-center">
                                            <label class="col-sm-3 col-form-label">Review</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="review" style="color: black; resize: none;" placeholder="Masukkan review"></textarea>
                                                <div>
                                                    @error('review')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ok</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('js')
<link rel="stylesheet" href="{{asset('a/lib/jquery.raty.css')}}">
</link>
<script src="{{asset('a/lib/jquery.raty.js')}}"></script>
<script>
    $(function() {
        $(".rating").raty({
            path: "{{asset('a/lib')}}",
            half: true,
            score: <?php echo $rating; ?>,
            click: function(score, evt) {
                Livewire.emit('rt', score);
            },
            hints: [
                ['bad 1/2', 'bad'],
                ['poor 1/2', 'poor'],
                ['regular 1/2', 'regular'],
                ['good 1/2', 'good'],
                ['gorgeous 1/2', 'gorgeous']
            ]
        });
    });
</script>
@endsection