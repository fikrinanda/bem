<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Review Kegiatan {{$pit}}</h4>
                        @if(!$iki && auth()->user()->level == "DPK")
                        <div class="d-sm-flex align-items-center">
                            <a href="/review/tambah/{{$lpj_id}}" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                        @elseif($iki && auth()->user()->level == "DPK")
                        <div class="d-sm-flex align-items-center">
                            <a href="/review/ubah/{{$lpj_id}}" class="btn btn-outline-primary rounded">Ubah</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div>
                                <label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>
                            </div>
                        </div>
                        @if(count($review) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>DPK</th>
                                        <th>Rating</th>
                                        <th>Review</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($review as $key => $b)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$review->firstItem() + $key}}</strong></td>
                                        <td style="width: 30%;">{{$b->dpk->nama}}</td>
                                        <td style="width: 30%;">
                                            <div id="review{{$review->firstItem()+$key}}" data-score="{{$b->rating}}">
                                            </div>
                                        </td>
                                        <td style="width: 30%;">{{$b->review}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$review->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<link rel="stylesheet" href="{{asset('a/lib/jquery.raty.css')}}">
</link>
<script src="{{asset('a/lib/jquery.raty.js')}}"></script>
<script>
    $("tr").each(function(index) {
        $('#review' + index).raty({
            path: "{{asset('a/lib')}}",
            readOnly: true
        });
    });
</script>
@endsection