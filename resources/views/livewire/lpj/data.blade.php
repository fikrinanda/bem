<div class="content-body">
    <style>
        #h:hover {
            text-decoration: underline;
        }
    </style>
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data LPJ</h4>
                        @if(auth()->user()->level == 'BEM' && $j != null)
                        <div class="d-sm-flex align-items-center">
                            <a href="/lpj/tambah" class="btn btn-outline-primary rounded">Ajukan</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($lpj) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        @if(auth()->user()->level == 'BEM')
                                        <th>ID</th>
                                        @elseif(auth()->user()->level == 'DPK')

                                        @endif
                                        <th>BEM</th>
                                        <th>Nama</th>
                                        <th>Tahun</th>
                                        <th>Status</th>
                                        <th>File</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($lpj as $key => $l)
                                    <tr>
                                        <td style="width: 9%;"><strong>{{$lpj->firstItem() + $key}}</strong></td>
                                        @if(auth()->user()->level == 'BEM')
                                        <td style="width: 13%;">{{$l->id}}</td>
                                        @elseif(auth()->user()->level == 'DPK')

                                        @endif
                                        <td style="width: 13%;">{{$l->bem->nama}}</td>
                                        <td style="width: 13%;">{{$l->proposal->nama}}</td>
                                        <td style="width: 13%;">{{$l->periode->tahun}}</td>
                                        <td style="width: 13%;">
                                            @if($l->status == 'Revisi')
                                            <a href="/revisi/lpj/{{$l->id}}" id="h">{{$l->status}}</a>
                                            @else
                                            {{$l->status}}
                                            @endif
                                        </td>
                                        <td style="width: 13%;">
                                            <div class="d-flex">
                                                <a href="/lpj/lihat/{{$l->id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-download"></i></a>
                                            </div>
                                        </td>
                                        @if(auth()->user()->level == 'BEM' && $l->status != 'Disetujui')
                                        <td style="width: 13%;">
                                            <div class="d-flex">
                                                <!-- <a href="/lpj/detail/{{$l->id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-image"></i></a> -->
                                                <a href="/lpj/ubah/{{$l->id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$l->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                        @elseif(auth()->user()->level == 'DPK' && $l->status != 'Disetujui')
                                        <td style="width: 13%;">
                                            <div class="d-flex">
                                                <a href="/lpj/revisi/{{$l->id}}" class="btn btn-danger shadow sharp mr-1">Revisi</a>
                                                <button wire:click="hapus('{{$l->id}}')" class="btn btn-info shadow sharp">Setuju</button>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$lpj->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>