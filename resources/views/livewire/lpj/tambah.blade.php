<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Mengajukan LPJ Kegiatan {{$nama}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row d-flex align-items-center">
                                            <label class="col-sm-3 col-form-label">File</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="file">
                                                <div>
                                                    @error('file')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="file">Uploading...</div>
                                            </div>
                                        </div>

                                        <div class="form-group row d-flex align-items-center">
                                            <label class="col-sm-3 col-form-label">Foto Kegiatan</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="foto" multiple>
                                                <div>
                                                    @error('foto.*')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="foto">Uploading...</div>
                                            </div>
                                        </div>

                                        @if (count($foto) > 0)
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Foto Kegiatan:</label>
                                            <div class="col-sm-9">
                                                @foreach($foto as $key => $value)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <img src="{{ $foto[$key]->temporaryUrl() }}" height="300" width="500">
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ajukan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>