<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah DPK</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Prodi</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="prodi" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Prodi</option>
                                                        @foreach($pd as $p)
                                                        <option value="{{$p->id}}">{{$p->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('prodi')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="foto2">
                                                <div>
                                                    @error('foto2')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="foto2">Uploading...</div>
                                                @if ($foto2)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <div>Photo Preview:</div>
                                                    <img src="{{ $foto2->temporaryUrl() }}" height="400" width="300">
                                                </div>
                                                @else
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <img src="/storage/{{$foto}}" height="400" width="300">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>