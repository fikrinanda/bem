<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$nama}}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                {{$nama}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Prodi</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                {{$prodi}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                {{$uname}}
                            </div>
                        </div>
                        <!-- <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Password</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                {{$password}}
                            </div>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>