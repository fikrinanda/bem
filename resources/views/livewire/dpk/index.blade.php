<div class="content-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb-5">
            <h2 class="text-black font-w600 mb-0">Profil BEM</h2>
            <img src="{{asset('assets/images/bem.png')}}" width="75" height="75" alt="">
        </div>

        <div class="row">
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <!-- <a href="/proposal/data"> -->
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$total}}</span></h3>
                                <p class="mb-0">Total Kegiatan</p>

                            </div>
                        </div>
                    </div>
                    <!-- </a> -->
                </div>
            </div>
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$laksana}}</span></h3>
                                <p class="mb-0">Terlaksana</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$ang}}</span></h3>
                                <p class="mb-0">Total Anggota BEM</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="card-body">
                    <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                        <div class=""><label>Periode <select wire:model="tahun">
                                    @foreach($periode as $p)
                                    <option value="{{$p->id}}">{{$p->tahun}}</option>
                                    @endforeach
                                </select></label></div>
                        <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                    </div>
                    <h3 class="my-3">Periode {{$s}}</h3>
                    @if(count($bem) > 0)
                    <div class="row">
                        @foreach($bem as $p)
                        <div class="col-lg-3">
                            <div class="card mb-3">
                                <div class="card-header text-center">
                                    <h5 class="card-title mx-auto"><a href="/bem/lihat/{{$p->bem->user->username}}">{{$p->bem->nama}}</a></h5>
                                </div>
                                <div class="card-body">
                                    <a href="/bem/lihat/{{$p->bem->user->username}}"><img class="card-img-bottom" src="/storage/{{$p->foto}}" width="100px" height="300px"></a>
                                </div>
                                <div class="card-footer">
                                    <h5 class="text-center">{{$p->jabatan}}</h5>
                                    @if($p->kementrian_id != null)
                                    <h5 class="text-center">{{$p->kementrian->nama}}</h5>
                                    @else
                                    <h5 class="text-center">&nbsp;</h5>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <h1>Tidak ada data</h1>
                    @endif


                </div>
            </div>
        </div>

        @if(count($lpj) > 0)
        <div class="row">
            @foreach($lpj as $k => $p)
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body p-4">
                        <h4 class="card-intro-title mb-3">{{$p->lpj->proposal->nama}}</h4>
                        <div id="carouselExampleIndicators{{$p->lpj_id}}" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach($foto[$p->lpj_id] as $key => $value)
                                <li data-target="#carouselExampleIndicators{{$p->lpj_id}}" data-slide-to="{{$key}}" class="@if($key==0) active @endif">
                                </li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach($foto[$p->lpj_id] as $key => $value)
                                <div class="carousel-item @if($key==0) active @endif">
                                    <img class="d-block w-100" src="/storage/{{ $value}}" alt="First slide">
                                </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators{{$p->lpj_id}}" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators{{$p->lpj_id}}" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>

</div>