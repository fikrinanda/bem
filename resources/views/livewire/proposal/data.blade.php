<div class="content-body">
    <style>
        #h:hover {
            text-decoration: underline;
        }
    </style>
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Proposal</h4>
                        @if(auth()->user()->level == 'BEM' && $j != null)
                        <div class="d-sm-flex align-items-center">
                            <a href="/proposal/tambah" class="btn btn-outline-primary rounded">Ajukan</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>
                                <label>Periode <select wire:model="pr">
                                        <option hidden>Filter periode</option>
                                        @if(auth()->user()->level == 'DPK' || auth()->user()->level == 'Admin')
                                        @foreach($periode as $p)
                                        <option value="{{$p->id}}">{{$p->tahun}}</option>
                                        @endforeach
                                        @elseif(auth()->user()->level == 'BEM')
                                        @foreach($periode as $p)
                                        <option value="{{$p->periode->id}}">{{$p->periode->tahun}}</option>
                                        @endforeach
                                        @endif
                                    </select></label>
                            </div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($proposal) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>BEM</th>
                                        <th>Kegiatan</th>
                                        <th>Tahun</th>
                                        <th>Status Proposal</th>
                                        <th>Status LPJ</th>
                                        @if(auth()->user()->level == 'BEM')
                                        <th>Detail</th>
                                        @elseif(auth()->user()->level == 'DPK')
                                        @if(auth()->user()->dpk->status != 'Tidak Aktif')
                                        <th>Detail</th>
                                        @endif
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($proposal as $key => $p)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$proposal->firstItem() + $key}}</strong></td>
                                        <td style="width: 15%;">{{$p->bem->nama}}</td>
                                        <td style="width: 15%;">{{$p->nama}}</td>
                                        <td style="width: 15%;">{{$p->periode->tahun}}</td>
                                        <td style="width: 15%;">
                                            <?php
                                            $status = App\Models\DetailProposal::where('proposal_id', $p->id)->orderBy('created_at', 'desc')->first();
                                            echo $status->status;
                                            ?>
                                        </td>
                                        <td style="width: 15%;">
                                            <?php
                                            $sosis = App\Models\LPJ::where('proposal_id', $p->id)->exists();
                                            if ($sosis) {
                                                $sosis2 = App\Models\LPJ::where('proposal_id', $p->id)->first()->id;
                                                $status = App\Models\DetailLPJ::where('lpj_id', $sosis2)->orderBy('created_at', 'desc')->first();
                                                echo $status->status;
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                        </td>

                                        @if(auth()->user()->level == 'BEM' && $p->status != 'Disetujui')
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/proposal/detail/{{$p->id}}" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">Proposal</a>
                                                <button type="button" wire:click="cek('{{$p->id}}')" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">LPJ</button>
                                                @if($status->status == "Setuju" && isset($sosis2))
                                                <a href="/review/data/{{$sosis2}}" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">Review</a> @endif
                                                <!-- <a href="/proposal/ubah/{{$p->id}}" class="btn btn-warning shadow sharp mr-1 d-flex justify-content-center align-items-center" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$p->id}}')" class="btn btn-danger shadow sharp d-flex justify-content-center align-items-center" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button> -->
                                            </div>
                                        </td>
                                        @elseif(auth()->user()->level == 'DPK' && $p->status != 'Disetujui' && auth()->user()->dpk->status != 'Tidak Aktif')
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/proposal/detail/{{$p->id}}" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">Proposal</a>
                                                <button type="button" wire:click="cek2('{{$p->id}}')" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">LPJ</button>
                                                @if($status->status == "Setuju" && isset($sosis2))
                                                <a href="/review/data/{{$sosis2}}" class="btn btn-primary mr-1 d-flex justify-content-center align-items-center btn-sm">Review</a> @endif
                                            </div>
                                        </td>
                                        @else
                                        @if(App\Models\LPJ::where('proposal_id', $p->id)->exists())
                                        @php
                                        $lpj = App\Models\LPJ::where('proposal_id', $p->id)->first();
                                        @endphp
                                        @if(auth()->user()->level == 'BEM' && $lpj->status != 'Disetujui')
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/lpj/ubah/{{$lpj->id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus2('{{$lpj->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                        @elseif(auth()->user()->level == 'DPK' && $lpj->status != 'Disetujui' && auth()->user()->dpk->status == 'Aktif')
                                        @php
                                        $lpj = App\Models\LPJ::where('proposal_id', $p->id)->first();
                                        @endphp
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/lpj/revisi/{{$lpj->id}}" class="btn btn-danger shadow sharp mr-1">Revisi</a>
                                                <button wire:click="hapus2('{{$lpj->id}}')" class="btn btn-info shadow sharp">Setuju</button>
                                            </div>
                                        </td>
                                        @endif
                                        @else
                                        @if(auth()->user()->level=='BEM')
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/lpj/tambah/{{$p->id}}" class="btn btn-primary shadow sharp mr-1">Ajukan LPJ</a>
                                            </div>
                                        </td>
                                        @endif
                                        @endif

                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$proposal->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>