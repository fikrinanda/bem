<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Revisi Proposal {{$nama}} Periode {{$periode}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="revisi" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Revisi</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="revisi" style="color: black; resize: none;" placeholder="Masukkan revisi"></textarea>
                                                <div>
                                                    @error('revisi')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Revisi</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>