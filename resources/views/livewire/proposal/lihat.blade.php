<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Detail Proposal {{$nama}} Tahun {{$periode}}
                                </h4>
                            </div>
                            <div class="card-header d-flex align-items-center">
                                <h4 class="card-title">Pembimbing {{$dpk}}</h4>
                                @if($utama == "Utama" && auth()->user()->level == "DPK" && $info == false)
                                <div wire:ignore>
                                    <label>Disposisi ke: <select wire:model="anu">
                                            <option hidden>Pilih DPK</option>
                                            @foreach($cad as $data)
                                            <option value="{{$data->id}}">{{$data->nama}}</option>
                                            @endforeach
                                        </select></label>
                                    <button wire:click="disposisi" class="btn btn-primary btn-sm">Ok</button>
                                </div>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    @if(auth()->user()->level == 'BEM')
                                    @foreach($detail as $key => $value)
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">File Proposal</label>
                                        <div class="col-sm-4">
                                            <a href="/proposal/unduh/{{$value->hash}}" class="btn btn-success btn-sm">Unduh</a>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">Diajukan pada tanggal</label>
                                        <div class="col-sm-4">
                                            {{$value->created_at->format('d, F Y H:i:s')}}
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">Status</label>
                                        <div class="col-sm-4">
                                            {{$value->status}}
                                        </div>
                                    </div>

                                    @if($value->status == "Revisi" || $value->status == "Sudah Revisi")
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">Revisi</label>
                                        <div class="col-sm-4">
                                            {{$value->revisi}}
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4">Direvisi pada tanggal</label>
                                        <div class="col-sm-8">
                                            {{$value->updated_at->format('d F Y H:i:s')}}
                                        </div>
                                    </div>
                                    @if($value->status == "Revisi")
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">File Revisi</label>
                                        <div class="col-sm-8">
                                            <input type="file" style="color: black;" wire:model="file">
                                            <div>
                                                @error('file')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="button" wire:click="revisi('{{$value->hash}}')" class="btn btn-primary ml-3">Revisi</button>
                                    </div>
                                    @endif
                                    @endif
                                    <hr class="mb-5" style="border: 1px solid black;">
                                    @endforeach

                                    @elseif(auth()->user()->level == 'DPK')
                                    @foreach($detail as $key => $value)
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">File Proposal</label>
                                        <div class="col-sm-4">
                                            <a href="/proposal/unduh/{{$value->hash}}" class="btn btn-success btn-sm">Unduh</a>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4 col-form-label">Diajukan pada tanggal</label>
                                        <div class="col-sm-4">
                                            {{$value->created_at->format('d, F Y H:i:s')}}
                                        </div>
                                    </div>

                                    @if($iya == auth()->user()->dpk->id)
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4">Status</label>
                                        <div class="col-sm-8">
                                            @if($value->status == "Menunggu Persetujuan")
                                            <div wire:ignore>
                                                <select wire:model="status" class="form-control" style="color: black;">
                                                    <option hidden>Pilih Status</option>
                                                    <option value="Revisi">Revisi</option>
                                                    <option value="Setuju">Setuju</option>
                                                </select>
                                            </div>
                                            <div>
                                                @error('status')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @else
                                            {{$value->status}}
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if($status == 'Revisi' || $value->status == "Revisi" || $value->status == "Sudah Revisi")
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4">Revisi</label>
                                        <div class="col-sm-8">
                                            @if($value->status == "Menunggu Persetujuan")
                                            <textarea class="form-control" rows="4" wire:model="revisi2" style="color: black; resize: none;" placeholder="Masukkan revisi"></textarea>
                                            <div>
                                                @error('revisi2')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            @elseif($value->status == "Revisi" || $value->status == "Sudah Revisi")
                                            {{$value->revisi}}
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if($value->status == "Revisi" || $value->status == "Sudah Revisi")
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4">Direvisi pada tanggal</label>
                                        <div class="col-sm-8">
                                            {{$value->updated_at->format('d F Y H:i:s')}}
                                        </div>
                                    </div>
                                    @elseif($value->status == "Setuju")
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-sm-4">Disetujui pada tanggal</label>
                                        <div class="col-sm-8">
                                            {{$value->updated_at->format('d F Y H:i:s')}}
                                        </div>
                                    </div>
                                    @endif

                                    @if($value->status == "Menunggu Persetujuan" && $iya == auth()->user()->dpk->id)
                                    <div class="d-flex justify-content-end">
                                        <button type="button" wire:click="revisi('{{$value->hash}}')" class="btn btn-primary ml-3">Ok</button>
                                    </div>
                                    @endif
                                    <hr class="mb-5" style="border: 1px solid black;">
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>