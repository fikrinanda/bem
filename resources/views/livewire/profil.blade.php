<div class="content-body">
    <div class="container-fluid">
        @if(auth()->user()->level == 'BEM')
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$user->bem->foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$user->bem->nama}}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->bem->nama}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Kementrian</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                @foreach($kementrian as $k)
                                @if($k->kementrian_id != null)
                                <h6>{{$k->kementrian->nama}} - {{$k->periode->tahun}}</h6>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Jabatan</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                @foreach($kementrian as $k)
                                <h6>{{$k->jabatan}} - {{$k->periode->tahun}}</h6>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Prodi</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->bem->prodi->nama}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->username}}</h6>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        @elseif(auth()->user()->level == 'DPK')
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$user->dpk->foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$user->dpk->nama}}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->dpk->nama}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Prodi</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->dpk->prodi->nama}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$user->username}}</h6>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        @endif
    </div>
</div>