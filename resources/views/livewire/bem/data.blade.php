<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data BEM</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/bem/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>

                            <div>
                                <label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>
                                <label>Filter periode <select wire:model="per">
                                        <option hidden>Pilih periode</option>
                                        @foreach($periode as $data)
                                        <option value="{{$data->id}}">{{$data->tahun}}</option>
                                        @endforeach
                                    </select></label>
                            </div>

                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($bem) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bem as $key => $b)
                                    <tr>
                                        <td style="width: 12%;"><strong>{{$bem->firstItem() + $key}}</strong></td>
                                        <td style="width: 22%;">{{$b->bem->id}}</td>
                                        <td style="width: 22%;">{{$b->bem->user->username}}</td>
                                        <td style="width: 22%;">{{$b->bem->nama}}</td>
                                        <td style="width: 22%;">
                                            <div class="d-flex">
                                                <a href="/bem/lihat/{{$b->bem->user->username}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/bem/ubah/{{$b->bem->user->username}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$b->bem->user->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$bem->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>