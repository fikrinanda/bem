<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$nama}}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$nama}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Kementrian</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                @foreach($kementrian as $k)
                                @if($k->kementrian_id != null)
                                <h6>{{$k->kementrian->nama}} - {{$k->periode->tahun}}</h6>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Jabatan</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                @foreach($kementrian as $k)
                                <h6>{{$k->jabatan}} - {{$k->periode->tahun}}</h6>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Prodi</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$prodi}}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                <h6>{{$uname}}</h6>
                            </div>
                        </div>
                        <!-- <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Password</h6>
                            </div>
                            <div class="col-sm-9 text-black">
                                {{$password}}
                            </div>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>