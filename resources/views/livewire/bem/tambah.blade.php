<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Tambah BEM Baru</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="basic-form">
                                            <form wire:submit.prevent="tambah" autocomplete="off">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Nama</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                        <div>
                                                            @error('nama')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Prodi</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="prodi" class="form-control" style="color: black;">
                                                                <option hidden>Pilih Prodi</option>
                                                                @foreach($pd as $p)
                                                                <option value="{{$p->id}}">{{$p->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('prodi')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Jabatan</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="jabatan" class="form-control" style="color: black;">
                                                                <option hidden>Pilih Jabatan</option>
                                                                <option value="Ketua">Ketua</option>
                                                                <option value="Wakil ketua">Wakil ketua</option>
                                                                <option value="Kementrian sekretaris kabinet">Kementrian sekretaris kabinet</option>
                                                                <option value="Kementrian keuangan">Kementrian keuangan</option>
                                                                <option value="Staff ahli">Staff ahli</option>
                                                                <option value="Staff muda">Staff muda</option>
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('jabatan')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Periode</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="periode" class="form-control" style="color: black;">
                                                                <option hidden>Pilih periode</option>
                                                                @foreach($periode2 as $p)
                                                                <option value="{{$p->id}}">{{$p->tahun}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('periode')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($jabatan == 'Staff ahli' || $jabatan == 'Staff muda')
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Kementrian</label>
                                                    <div class="col-sm-9">
                                                        <div>
                                                            <select wire:model="kementrian" class="form-control" style="color: black;">
                                                                <option hidden>Pilih Kementrian</option>
                                                                @foreach($ktm as $k)
                                                                <option value="{{$k->id}}">{{$k->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('kementrian')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Username</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan username" wire:model="username">
                                                        <div>
                                                            @error('username')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Password</label>
                                                    <div class="col-sm-9">
                                                        <input type="password" class="form-control" style="color: black;" placeholder="Masukkan password" wire:model="password">
                                                        <div>
                                                            @error('password')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Foto</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" style="color: black;" wire:model="foto">
                                                        <div>
                                                            @error('foto')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div wire:loading wire:target="foto">Uploading...</div>
                                                        @if ($foto)
                                                        <div class="mt-3" style="object-fit: cover;">
                                                            <div>Photo Preview:</div>
                                                            <img src="{{ $foto->temporaryUrl() }}" height="400" width="300">
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Tambah BEM Periode</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="basic-form">
                                            <form wire:submit.prevent="tambah2" autocomplete="off">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">BEM</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="bem" class="form-control" style="color: black;">
                                                                <option hidden>Pilih BEM</option>
                                                                @foreach($bem2 as $p)
                                                                <option value="{{$p->id}}">{{$p->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('bem')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Jabatan</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="jabatan2" class="form-control" style="color: black;">
                                                                <option hidden>Pilih Jabatan</option>
                                                                <option value="Ketua">Ketua</option>
                                                                <option value="Wakil ketua">Wakil ketua</option>
                                                                <option value="Kementrian sekretaris kabinet">Kementrian sekretaris kabinet</option>
                                                                <option value="Kementrian keuangan">Kementrian keuangan</option>
                                                                <option value="Staff ahli">Staff ahli</option>
                                                                <option value="Staff muda">Staff muda</option>
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('jabatan2')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Periode</label>
                                                    <div class="col-sm-9">
                                                        <div wire:ignore>
                                                            <select wire:model="periode3" class="form-control" style="color: black;">
                                                                <option hidden>Pilih periode</option>
                                                                @foreach($periode2 as $p)
                                                                <option value="{{$p->id}}">{{$p->tahun}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('periode3')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($jabatan2 == 'Staff ahli' || $jabatan2 == 'Staff muda')
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Kementrian</label>
                                                    <div class="col-sm-9">
                                                        <div>
                                                            <select wire:model="kementrian2" class="form-control" style="color: black;">
                                                                <option hidden>Pilih Kementrian</option>
                                                                @foreach($ktm as $k)
                                                                <option value="{{$k->id}}">{{$k->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div>
                                                            @error('kementrian2')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>