<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_proposal', function (Blueprint $table) {
            $table->string('proposal_id', 10);
            $table->string('hash', 32);
            $table->string('file', 250);
            $table->string('status', 50);
            $table->longText('revisi')->nullable();
            $table->timestamps();

            $table->foreign('proposal_id')->references('id')->on('proposal')->onDelete('cascade');
        });

        Schema::create('detail_lpj', function (Blueprint $table) {
            $table->string('lpj_id', 10);
            $table->string('hash', 32);
            $table->string('file', 250);
            $table->string('status', 50);
            $table->longText('revisi')->nullable();
            $table->timestamps();

            $table->foreign('lpj_id')->references('id')->on('lpj')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_proposal', function (Blueprint $table) {
            $table->dropForeign(['proposal_id']);
            $table->dropColumn('proposal_id');
        });
        Schema::dropIfExists('detail_proposal');

        Schema::table('detail_lpj', function (Blueprint $table) {
            $table->dropForeign(['lpj_id']);
            $table->dropColumn('lpj_id');
        });
        Schema::dropIfExists('detail_lpj');
    }
}
