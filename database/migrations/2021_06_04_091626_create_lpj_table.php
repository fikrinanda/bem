<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLpjTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lpj', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('bem_id', 10);
            $table->string('proposal_id', 10);
            $table->string('file', 250);
            $table->string('status', 50);
            $table->timestamps();

            $table->foreign('bem_id')->references('id')->on('bem')->onDelete('cascade');
            $table->foreign('proposal_id')->references('id')->on('proposal')->onDelete('cascade');
        });

        Schema::create('foto_kegiatan', function (Blueprint $table) {
            $table->string('lpj_id', 10);
            $table->string('foto', 200);
            $table->timestamps();

            $table->foreign('lpj_id')->references('id')->on('lpj')->onDelete('cascade');
        });

        Schema::create('revisi_lpj', function (Blueprint $table) {
            $table->string('lpj_id', 10);
            $table->longText('revisi');
            $table->timestamps();

            $table->foreign('lpj_id')->references('id')->on('lpj')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revisi_lpj', function (Blueprint $table) {
            $table->dropForeign(['lpj_id']);
            $table->dropColumn('lpj_id');
        });
        Schema::table('foto_kegiatan', function (Blueprint $table) {
            $table->dropForeign(['lpj_id']);
            $table->dropColumn('lpj_id');
        });
        Schema::table('lpj', function (Blueprint $table) {
            $table->dropForeign(['bem_id']);
            $table->dropColumn('bem_id');
            $table->dropForeign(['proposal_id']);
            $table->dropColumn('proposal_id');
        });
        Schema::dropIfExists('revisi_lpj');
        Schema::dropIfExists('foto_kegiatan');
        Schema::dropIfExists('lpj');
    }
}
