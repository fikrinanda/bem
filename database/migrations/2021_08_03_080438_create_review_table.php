<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->string('dpk_id', 10);
            $table->string('lpj_id', 10);
            $table->string('rating', 10);
            $table->longText('review')->nullable();
            $table->timestamps();

            $table->foreign('dpk_id')->references('id')->on('dpk')->onDelete('cascade');

            $table->foreign('lpj_id')->references('id')->on('lpj')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('review', function (Blueprint $table) {
            $table->dropForeign(['dpk_id']);
            $table->dropColumn('dpk_id');
            $table->dropForeign(['lpj_id']);
            $table->dropColumn('lpj_id');
        });
        Schema::dropIfExists('review');
    }
}
