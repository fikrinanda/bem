<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('username', 20);
            $table->string('password', 100);
            $table->string('level', 5);
            $table->timestamps();
        });

        Schema::create('prodi', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('nama', 50);
            $table->timestamps();
        });

        Schema::create('dpk', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('prodi_id', 10);
            $table->string('nama', 50);
            $table->string('foto', 200);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('prodi_id')->references('id')->on('prodi')->onDelete('cascade');
        });

        Schema::create('kementrian', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('nama', 50);
            $table->timestamps();
        });

        Schema::create('bem', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('kementrian_id', 10);
            $table->string('prodi_id', 10);
            $table->string('nama', 50);
            $table->string('foto', 200);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('kementrian_id')->references('id')->on('kementrian')->onDelete('cascade');
            $table->foreign('prodi_id')->references('id')->on('prodi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bem', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['kementrian_id']);
            $table->dropColumn('kementrian_id');
            $table->dropForeign(['prodi_id']);
            $table->dropColumn('prodi_id');
        });

        Schema::table('dpk', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['prodi_id']);
            $table->dropColumn('prodi_id');
        });

        Schema::dropIfExists('users');
        Schema::dropIfExists('prodi');
        Schema::dropIfExists('dpk');
        Schema::dropIfExists('bem');
        Schema::dropIfExists('kementrian');
    }
}
