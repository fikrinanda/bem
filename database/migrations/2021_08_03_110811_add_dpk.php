<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDpk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal', function (Blueprint $table) {
            $table->string('dpk_id', 10)->after('periode_id')->nullable();
            $table->foreign('dpk_id')->references('id')->on('dpk')->onDelete('cascade');
        });

        Schema::table('lpj', function (Blueprint $table) {
            $table->string('dpk_id', 10)->after('periode_id')->nullable();
            $table->foreign('dpk_id')->references('id')->on('dpk')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal', function (Blueprint $table) {
            $table->dropForeign(['dpk_id']);
            $table->dropColumn('dpk_id');
        });

        Schema::table('lpj', function (Blueprint $table) {
            $table->dropForeign(['dpk_id']);
            $table->dropColumn('dpk_id');
        });
    }
}
