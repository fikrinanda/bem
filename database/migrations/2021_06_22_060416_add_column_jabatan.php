<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bem', function (Blueprint $table) {
            $table->string('kementrian_id', 10)->nullable()->change();
            $table->string('jabatan', 50)->nullable()->after('prodi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bem', function (Blueprint $table) {
            $table->string('kementrian_id', 10)->change();
            $table->dropColumn('jabatan');
        });
    }
}
