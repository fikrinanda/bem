<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteKet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bem', function (Blueprint $table) {
            $table->dropForeign(['kementrian_id']);
            $table->dropColumn('kementrian_id');
            $table->dropForeign(['prodi_id']);
            $table->dropColumn('prodi_id');
            $table->dropColumn('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bem', function (Blueprint $table) {
        });
    }
}
