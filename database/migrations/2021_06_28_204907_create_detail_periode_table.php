<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPeriodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_periode', function (Blueprint $table) {
            $table->string('bem_id', 10);
            $table->string('kementrian_id', 10);
            $table->string('prodi_id', 10);
            $table->string('jabatan', 50);
            $table->string('periode_id', 10);
            $table->string('hash', 32);
            $table->timestamps();

            $table->foreign('bem_id')->references('id')->on('bem')->onDelete('cascade');
            $table->foreign('kementrian_id')->references('id')->on('kementrian')->onDelete('cascade');
            $table->foreign('prodi_id')->references('id')->on('prodi')->onDelete('cascade');
            $table->foreign('periode_id')->references('id')->on('periode')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_periode', function (Blueprint $table) {
            $table->dropForeign(['bem_id']);
            $table->dropColumn('bem_id');
            $table->dropForeign(['kementrian_id']);
            $table->dropColumn('kementrian_id');
            $table->dropForeign(['prodi_id']);
            $table->dropColumn('prodi_id');
            $table->dropForeign(['periode_id']);
            $table->dropColumn('periode_id');
        });
        Schema::dropIfExists('detail_periode');
    }
}
