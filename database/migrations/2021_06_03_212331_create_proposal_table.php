<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('bem_id', 10);
            $table->string('nama', 50);
            $table->string('tahun', 10);
            $table->string('file', 250);
            $table->string('status', 50);
            $table->timestamps();

            $table->foreign('bem_id')->references('id')->on('bem')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal', function (Blueprint $table) {
            $table->dropForeign(['bem_id']);
            $table->dropColumn('bem_id');
        });
        Schema::dropIfExists('proposal');
    }
}
