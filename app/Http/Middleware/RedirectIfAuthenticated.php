<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if (auth()->user()->level == 'Admin') {
                    return redirect()->intended('/admin');
                } else if (auth()->user()->level == 'DPK') {
                    return redirect()->intended('/dpk');
                } else if (auth()->user()->level == 'BEM') {
                    return redirect()->intended('/bem');
                }
            }
        }

        return $next($request);
    }
}
