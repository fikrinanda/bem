<?php

namespace App\Http\Livewire\Bem;

use App\Models\BEM;
use App\Models\Detail;
use App\Models\Kementrian;
use App\Models\Periode;
use App\Models\Prodi;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $prodi;
    public $jabatan;
    public $kementrian;
    public $periode;
    public $foto;
    public $username;
    public $password;
    public $bem;
    public $jabatan2;
    public $kementrian2;
    public $periode3;
    protected $listeners = ['berhasil'];


    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            'jabatan' => 'required',
            'periode' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            'jabatan' => 'required',
            'periode' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
        ]);

        if ($this->jabatan == 'Staff muda' || $this->jabatan == 'Staff ahli') {
            $this->validate([
                'kementrian' => 'required',
            ]);
        }

        $foto = $this->foto->store('images/bem/foto', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'BEM',
        ]);

        $user = User::where('username', $this->username)->first();

        $x2 = BEM::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "BM" . sprintf("%04s", $y2);

        BEM::create([
            'id' => $z2,
            'user_id' => $user->id,
            'prodi_id' => $this->prodi,
            'nama' => $this->nama,
            'foto' => $foto,
        ]);

        Detail::create([
            'bem_id' => $z2,
            'jabatan' => $this->jabatan,
            'kementrian_id' => $this->kementrian ?? null,
            'periode_id' => $this->periode,
            'hash' => Str::random(32)
        ]);

        $this->showModal("Data BEM $this->nama berhasil ditambahkan");
    }

    public function tambah2()
    {
        $this->validate([
            'jabatan2' => 'required',
            'periode3' => 'required',
        ]);

        if ($this->jabatan2 == 'Staff muda' || $this->jabatan2 == 'Staff ahli') {
            $this->validate([
                'kementrian2' => 'required',
            ]);
        }

        $test = Detail::where('bem_id', $this->bem)->where('periode_id', $this->periode3)->exists();

        if ($test) {
            $nama = BEM::find($this->bem)->nama;
            $tahun = Periode::find($this->periode3)->tahun;
            $this->showAlert("Data BEM $nama Periode $tahun sudah ada");
        } else {
            if (Detail::where('bem_id', $this->bem)->count() >= 2) {
                $nama = BEM::find($this->bem)->nama;
                $this->showAlert("BEM $nama Periode sudah menjabat selama dua periode");
            } else {


                Detail::create([
                    'bem_id' => $this->bem,
                    'jabatan' => $this->jabatan2,
                    'kementrian_id' => $this->kementrian2 ?? null,
                    'periode_id' => $this->periode3,
                    'hash' => Str::random(32)
                ]);

                $nama = BEM::find($this->bem)->nama;
                $tahun = Periode::find($this->periode3)->tahun;
                $this->showModal("Data BEM $nama Periode $tahun berhasil ditambahkan");
            }
        }
    }

    public function showModal($text)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $text,
        ]);
    }

    public function showAlert($text)
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => $text,
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/bem/data');
    }

    public function render()
    {
        $ktm = Kementrian::get();
        $pd = Prodi::get();
        $periode2 = Periode::get();
        $bem2 = BEM::get();
        return view('livewire.bem.tambah', compact(['ktm', 'pd', 'periode2', 'bem2']))->extends('layouts.admin', ['title' => 'Tambah BEM'])->section('content');
    }
}
