<?php

namespace App\Http\Livewire\Bem;

use App\Models\BEM;
use App\Models\Detail;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $kementrian;
    public $nama;
    public $prodi;
    public $foto;
    public $uname;
    public $password;
    public $i;

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $bem = BEM::where('user_id', $user->id)->first();
            if ($bem) {
                $this->i = $bem->id;
                $this->kementrian = Detail::where('bem_id', $bem->id)->get();
                $this->nama = $bem->nama;
                $this->prodi = $bem->prodi->nama;
                $this->foto = $bem->foto;
                $this->uname = $user->username;
                $this->password = $user->password;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        $level = auth()->user()->level;
        return view('livewire.bem.lihat')->extends("layouts.$level", ['title' => 'Lihat BEM'])->section('content');
    }
}
