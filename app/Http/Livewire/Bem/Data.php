<?php

namespace App\Http\Livewire\Bem;

use App\Models\BEM;
use App\Models\Detail;
use App\Models\Periode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $per = '';
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function mount()
    {
        $tahun = Carbon::now()->format('Y');
        $thn = $tahun . '/' . ($tahun + 1);
        $periode = Periode::where('tahun', $thn)->first();
        $this->per = $periode->id;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->bem->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->bem->nama;
        $bem = BEM::where('user_id', $id)->first();
        Storage::disk('public')->delete($bem->foto);
        $user->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data BEM $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus BEM $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $bem = Detail::selectRaw('detail_periode.*')->join('bem', 'bem.id', '=', 'detail_periode.bem_id')->where('nama', 'like', '%' . $this->search . '%')->where('periode_id', $this->per)->paginate($this->perPage);
        $periode = Periode::get();
        return view('livewire.bem.data', compact(['bem', 'periode']))->extends('layouts.admin', ['title' => 'Data BEM'])->section('content');
    }
}
