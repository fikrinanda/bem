<?php

namespace App\Http\Livewire\Bem;

use App\Models\BEM;
use App\Models\Detail;
use App\Models\Kementrian;
use App\Models\Prodi;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $kementrian;
    public $prodi;
    public $foto;
    public $foto2;
    public $i;
    protected $listeners = ['berhasil'];

    protected $rules = [
        'kementrian.*.kementrian_id' => 'required',
    ];

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $bem = BEM::where('user_id', $user->id)->first();
            if ($bem) {
                $this->i = $bem->id;
                $this->nama = $bem->nama;
                $this->prodi = $bem->prodi_id;
                $this->kementrian = Detail::where('bem_id', $bem->id)->get();
                $this->foto = $bem->foto;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/bem/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        BEM::where('id', $this->i)->update([
            'nama' => $this->nama,
            // 'kementrian_id' => $this->kementrian,
            'prodi_id' => $this->prodi,
            'foto' => $foto2,
        ]);

        foreach ($this->kementrian as $data) {
            Detail::where('hash', $data->hash)->update([
                'kementrian_id' => $data['kementrian_id']
            ]);
        }

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data BEM $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/bem/data');
    }

    public function render()
    {
        $ktm = Kementrian::get();
        $pd = Prodi::get();
        return view('livewire.bem.ubah', compact(['ktm', 'pd']))->extends('layouts.admin', ['title' => 'Ubah BEM'])->section('content');
    }
}
