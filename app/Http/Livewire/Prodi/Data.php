<?php

namespace App\Http\Livewire\Prodi;

use App\Models\Prodi;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $prd = Prodi::find($id);
        $this->showConfirmation($prd->id, $prd->nama);
    }

    public function hancur($id)
    {
        $prd = Prodi::find($id);
        $nama = $prd->nama;
        $prd->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Prodi $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Prodi $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $prodi = Prodi::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.prodi.data', compact(['prodi']))->extends('layouts.admin', ['title' => 'Data Prodi'])->section('content');
    }
}
