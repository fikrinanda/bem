<?php

namespace App\Http\Livewire\Prodi;

use App\Models\Prodi;
use Livewire\Component;

class Tambah extends Component
{
    public $nama;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:prodi,nama',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:prodi,nama',
        ]);

        $x = Prodi::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "PD" . sprintf("%04s", $y);

        Prodi::create([
            'id' => $z,
            'nama' => $this->nama,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Prodi $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/prodi/data');
    }

    public function render()
    {
        return view('livewire.prodi.tambah')->extends('layouts.admin', ['title' => 'Tambah Prodi'])->section('content');
    }
}
