<?php

namespace App\Http\Livewire\Proposal;

use App\Models\Detail;
use App\Models\DetailProposal;
use App\Models\DPK;
use App\Models\Proposal;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    // public $tahun;
    public $file;
    public $t;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $a = Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->kementrian_id;
        if ($a == null) {
            abort('404');
        }
        $this->t = Carbon::now()->format('Y');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            // 'tahun' => "required|integer|min:$this->t",
            'file' => 'required',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required',
            // 'tahun' => "required|integer|min:$this->t",
            'file' => 'required',
        ]);

        $file = $this->file->store('file/proposal', 'public');

        $x = Proposal::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "PS" . sprintf("%04s", $y);

        Proposal::create([
            'id' => $z,
            'bem_id' => auth()->user()->bem->id,
            'nama' => $this->nama,
            'periode_id' => Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->periode_id,
            'dpk_id' => DPK::where('status', 'Utama')->first()->id,
        ]);

        DetailProposal::create([
            'proposal_id' => $z,
            'hash' => Str::random(32),
            'file' => $file,
            'status' => "Menunggu Persetujuan",
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Proposal $this->nama berhasil diajukan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        return view('livewire.proposal.tambah')->extends('layouts.bem', ['title' => 'Mengajukan Proposal'])->section('content');
    }
}
