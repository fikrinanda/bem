<?php

namespace App\Http\Livewire\Proposal;

use App\Models\Detail;
use App\Models\DetailProposal;
use App\Models\LPJ;
use App\Models\Periode;
use App\Models\Proposal;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $j;
    public $pr;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal', 'yakin2' => 'hancur2', 'batal2'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        if (auth()->user()->level == 'BEM') {
            $this->j = Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->kementrian_id;
            $this->pr = Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->periode_id;
        } else {
            $this->pr = Periode::orderBy('created_at', 'desc')->first()->id;
        }
    }

    public function cek($id)
    {
        $test = DetailProposal::where('proposal_id', $id)->where('status', 'Setuju')->exists();
        if ($test) {
            $test2 = LPJ::where('proposal_id', $id);
            if ($test2->exists()) {
                $rose = $test2->first()->id;
                return redirect()->to("/lpj/detail/$rose");
            } else {
                return redirect()->to("/lpj/tambah/$id");
            }
        } else {
            $this->showAlert();
        }
    }

    public function cek2($id)
    {
        $test = DetailProposal::where('proposal_id', $id)->where('status', 'Setuju')->exists();
        if ($test) {
            $test2 = LPJ::where('proposal_id', $id);
            if ($test2->exists()) {
                $rose = $test2->first()->id;
                return redirect()->to("/lpj/detail/$rose");
            } else {
                $this->showAlert2();
            }
        } else {
            $this->showAlert();
        }
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => 'Proposal belum disetujui',
            'timeout' => 3000
        ]);
    }

    public function showAlert2()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => 'BEM belum mengajukan LPJ',
            'timeout' => 3000
        ]);
    }

    public function hapus($id)
    {
        $proposal = Proposal::find($id);
        if (auth()->user()->level == 'DPK') {
            $this->showConfirmation($proposal->id, $proposal->nama);
        } else if (auth()->user()->level == 'BEM') {
            $this->showConfirmation2($proposal->id, $proposal->nama);
        }
    }

    public function hapus2($id)
    {
        $proposal = LPJ::find($id);
        if (auth()->user()->level == 'DPK') {
            $this->showConfirmation3($proposal->id, $proposal->proposal->nama);
        } else if (auth()->user()->level == 'BEM') {
            $this->showConfirmation4($proposal->id, $proposal->proposal->nama);
        }
    }

    public function hancur($id)
    {
        $nm = Proposal::find($id)->nama;
        if (auth()->user()->level == 'DPK') {
            Proposal::where('id', $id)->update([
                'status' => 'Disetujui',
            ]);
            $this->showModal($nm);
        } else if (auth()->user()->level == 'BEM') {
            Proposal::where('id', $id)->delete();
            $this->showModal2($nm);
        }
    }

    public function hancur2($id)
    {
        $nm = LPJ::find($id)->proposal->nama;
        if (auth()->user()->level == 'DPK') {
            LPJ::where('id', $id)->update([
                'status' => 'Disetujui',
            ]);
            $this->showModal3($nm);
        } else if (auth()->user()->level == 'BEM') {
            LPJ::where('id', $id)->delete();
            $this->showModal4($nm);
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function batal2()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Proposal $nama berhasil disetujui",
        ]);
    }

    public function showModal2($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Proposal $nama berhasil dihapus",
        ]);
    }

    public function showModal3($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $nama berhasil disetujui",
        ]);
    }

    public function showModal4($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menyetujui Proposal $nama?",
            'text'        => "",
            'confirmText' => 'Ya, setuju!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin meghapus Proposal $nama?",
            'text'        => "",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation3($id, $nama)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin menyetujui LPJ $nama?",
            'text'        => "",
            'confirmText' => 'Ya, setuju!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation4($id, $nama)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin meghapus LPJ $nama?",
            'text'        => "",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        if (auth()->user()->level == 'DPK') {
            $proposal = Proposal::where('nama', 'like', '%' . $this->search . '%')->where('periode_id', $this->pr)->paginate($this->perPage);
            $periode = Periode::get();
            return view('livewire.proposal.data', compact(['proposal', 'periode']))->extends('layouts.dpk', ['title' => 'Data Proposal'])->section('content');
        } else if (auth()->user()->level == 'BEM') {
            $proposal = Proposal::where('bem_id', auth()->user()->bem->id)->where('periode_id', $this->pr)->where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
            $periode = Detail::where('bem_id', auth()->user()->bem->id)->get();
            return view('livewire.proposal.data', compact(['proposal', 'periode']))->extends('layouts.bem', ['title' => 'Data Proposal'])->section('content');
        } else {
            $proposal = Proposal::where('nama', 'like', '%' . $this->search . '%')->where('periode_id', $this->pr)->paginate($this->perPage);
            $periode = Periode::get();
            return view('livewire.proposal.data', compact(['proposal', 'periode']))->extends('layouts.admin', ['title' => 'Data Proposal'])->section('content');
        }
    }
}
