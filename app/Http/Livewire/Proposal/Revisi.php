<?php

namespace App\Http\Livewire\Proposal;

use App\Models\Proposal;
use App\Models\Revisi as ModelsRevisi;
use Livewire\Component;

class Revisi extends Component
{
    public $i;
    public $nama;
    public $periode;
    public $revisi;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = Proposal::where('id', $id)->exists();
        if ($test) {
            $this->i = $id;
            $proposal = Proposal::find($id);
            $this->nama = $proposal->nama;
            $this->periode = $proposal->periode->tahun;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'revisi' => 'required'
        ]);
    }

    public function revisi()
    {
        $this->validate([
            'revisi' => 'required'

        ]);

        ModelsRevisi::create([
            'proposal_id' => $this->i,
            'revisi' => $this->revisi,
        ]);

        Proposal::where('id', $this->i)->update([
            'status' => 'Revisi',
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Proposal $this->nama berhasil direvisi",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        return view('livewire.proposal.revisi')->extends('layouts.dpk', ['title' => 'Revisi Proposal'])->section('content');
    }
}
