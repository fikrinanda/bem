<?php

namespace App\Http\Livewire\Proposal;

use App\Models\DetailProposal;
use App\Models\DPK;
use App\Models\Proposal;
use App\Models\Revisi;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Lihat extends Component
{
    use WithFileUploads;

    public $i;
    public $nama;
    public $periode;
    public $file;
    public $status;
    public $revisi2;
    public $detail;
    public $dpk;
    public $utama;
    public $cad;
    public $anu;
    public $info;
    public $iya;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = Proposal::where('id', $id)->exists();
        if ($test) {
            $this->i = $id;
            $proposal = Proposal::find($id);
            $this->nama = $proposal->nama;
            $this->periode = $proposal->periode->tahun;
            $this->detail = DetailProposal::where('proposal_id', $id)->get();
            $this->dpk = $proposal->dpk->nama;
            $this->info = DetailProposal::where('proposal_id', $id)->where('status', 'Setuju')->exists();
            $this->iya = $proposal->dpk_id;
            if (auth()->user()->level == "DPK") {
                $this->utama = DPK::find(auth()->user()->dpk->id)->status;
                $this->cad = DPK::where('status',  "Aktif")->where('status', '!=', 'Utama')->get();
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        if (auth()->user()->level == "DPK") {
            if ($this->status == "Revisi") {
                $this->validateOnly($field, [
                    'revisi2' => 'required',
                ]);
            }
            $this->validateOnly($field, [
                'status' => 'required',
            ]);
        } else if (auth()->user()->level == 'BEM') {
            $this->validateOnly($field, [
                'file' => 'required',
            ]);
        }
    }

    public function disposisi()
    {
        Proposal::where('id', $this->i)->update([
            'dpk_id' => $this->anu
        ]);

        $this->showModal("");
    }

    public function revisi($hash)
    {
        if (auth()->user()->level == "DPK") {
            if ($this->status == "Revisi") {
                $this->validate([
                    'revisi2' => 'required',
                ]);
            }

            $this->validate([
                'status' => 'required',
            ]);

            DetailProposal::where('hash', $hash)->update([
                'status' => $this->status,
                'revisi' => $this->revisi2 ?? null
            ]);

            if ($this->status == "Revisi") {
                $weekend = "direvisi";
            } else if ($this->status == "Setuju") {
                $weekend = "disetujui";
            }

            $this->showModal("Proposal $this->nama berhasil $weekend");
        } else if (auth()->user()->level == 'BEM') {
            $this->validate([
                'file' => 'required',
            ]);

            $file = $this->file->store('file/proposal', 'public');

            DetailProposal::where('hash', $hash)->update([
                'status' => "Sudah Revisi"
            ]);

            DetailProposal::create([
                'proposal_id' => $this->i,
                'hash' => Str::random(32),
                'file' => $file,
                'status' => "Menunggu Persetujuan"
            ]);

            $this->showModal("Proposal $this->nama berhasil direvisi");
        }
    }

    public function showModal($text)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        if (auth()->user()->level == 'DPK') {
            return view('livewire.proposal.lihat')->extends('layouts.dpk', ['title' => 'Detail Proposal'])->section('content');
        } else if (auth()->user()->level == 'BEM') {
            return view('livewire.proposal.lihat')->extends('layouts.bem', ['title' => 'Detail Proposal'])->section('content');
        }
    }
}
