<?php

namespace App\Http\Livewire\Proposal;

use App\Models\Proposal;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $i;
    public $nama;
    public $file;
    public $file2;
    public $t;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $this->i = $id;
        $this->t = Carbon::now()->format('Y');

        $test = Proposal::where('id', $id)->exists();
        if ($test) {
            $p = Proposal::find($id);
            $this->nama = $p->nama;
            $this->file = $p->file;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            // 'file' => 'required',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required',
        ]);

        if ($this->file2) {
            Storage::disk('public')->delete($this->file);
            $file2 = $this->file2->store('file/proposal', 'public');
        } else {
            $file2 = $this->file ?? null;
        }

        Proposal::where('id', $this->i)->update([
            'nama' => $this->nama,
            'file' => $file2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Proposal $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        return view('livewire.proposal.ubah')->extends('layouts.bem', ['title' => 'Ubah Proposal'])->section('content');
    }
}
