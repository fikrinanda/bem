<?php

namespace App\Http\Livewire;

use App\Models\Detail;
use App\Models\User;
use Livewire\Component;

class Profil extends Component
{
    public $user;
    public $kementrian;

    public function mount()
    {
        if (auth()->user()->level == 'DPK') {
            $this->user = User::find(auth()->user()->id);
        } else if (auth()->user()->level == 'BEM') {
            $this->user = User::find(auth()->user()->id);
            $this->kementrian = Detail::where('bem_id', auth()->user()->bem->id)->get();
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'DPK') {
            return view('livewire.profil')->extends('layouts.dpk', ['title' => 'Profil'])->section('content');
        } else if (auth()->user()->level == 'BEM') {
            return view('livewire.profil')->extends('layouts.bem', ['title' => 'Profil'])->section('content');
        }
    }
}
