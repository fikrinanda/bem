<?php

namespace App\Http\Livewire\Lpj;

use App\Models\DetailLPJ;
use App\Models\DPK;
use App\Models\Foto;
use App\Models\LPJ;
use App\Models\Revisi2;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Lihat extends Component
{
    use WithFileUploads;

    public $i;
    public $nama;
    public $periode;
    public $file;
    public $status;
    public $revisi2;
    public $detail;
    public $foto;
    public $dpk;
    public $utama;
    public $cad;
    public $anu;
    public $info;
    public $iya;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = LPJ::where('id', $id)->exists();
        if ($test) {
            $this->i = $id;
            $proposal = LPJ::find($id);
            $this->nama = $proposal->proposal->nama;
            $this->periode = $proposal->proposal->periode->tahun;
            $this->detail = DetailLPJ::where('lpj_id', $id)->get();
            $this->foto = Foto::where('lpj_id', $id)->get();
            $this->dpk = $proposal->dpk->nama;
            $this->info = DetailLPJ::where('lpj_id', $id)->where('status', 'Setuju')->exists();
            $this->iya = $proposal->dpk_id;
            if (auth()->user()->level == "DPK") {
                $this->utama = DPK::find(auth()->user()->dpk->id)->status;
                $this->cad = DPK::where('status',  "Aktif")->where('status', '!=', 'Utama')->get();
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        if (auth()->user()->level == "DPK") {
            if ($this->status == "Revisi") {
                $this->validateOnly($field, [
                    'revisi2' => 'required',
                ]);
            }
            $this->validateOnly($field, [
                'status' => 'required',
            ]);
        } else if (auth()->user()->level == 'BEM') {
            $this->validateOnly($field, [
                'file' => 'required',
            ]);
        }
    }

    public function disposisi()
    {
        LPJ::where('id', $this->i)->update([
            'dpk_id' => $this->anu
        ]);

        $this->showModal("");
    }

    public function revisi($hash)
    {
        if (auth()->user()->level == "DPK") {
            if ($this->status == "Revisi") {
                $this->validate([
                    'revisi2' => 'required',
                ]);
            }

            $this->validate([
                'status' => 'required',
            ]);

            DetailLPJ::where('hash', $hash)->update([
                'status' => $this->status,
                'revisi' => $this->revisi2 ?? null
            ]);

            if ($this->status == "Revisi") {
                $weekend = "direvisi";
            } else if ($this->status == "Setuju") {
                $weekend = "disetujui";
            }

            $this->showModal("LPJ $this->nama berhasil $weekend");
        } else if (auth()->user()->level == 'BEM') {
            $this->validate([
                'file' => 'required',
            ]);

            $file = $this->file->store('file/lpj', 'public');

            DetailLPJ::where('hash', $hash)->update([
                'status' => "Sudah Revisi"
            ]);

            DetailLPJ::create([
                'lpj_id' => $this->i,
                'hash' => Str::random(32),
                'file' => $file,
                'status' => "Menunggu Persetujuan"
            ]);

            $this->showModal("LPJ $this->nama berhasil direvisi");
        }
    }

    public function showModal($text)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        $revisi = Revisi2::where('lpj_id', $this->i)->get();
        if (auth()->user()->level == 'DPK') {
            return view('livewire.lpj.lihat', compact(['revisi']))->extends('layouts.dpk', ['title' => 'Detail LPJ'])->section('content');
        } else if (auth()->user()->level == 'BEM') {
            return view('livewire.lpj.lihat', compact(['revisi']))->extends('layouts.bem', ['title' => 'Detail LPJ'])->section('content');
        }
    }
}
