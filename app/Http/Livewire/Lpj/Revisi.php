<?php

namespace App\Http\Livewire\Lpj;

use App\Models\Foto;
use App\Models\LPJ;
use App\Models\Revisi2;
use Livewire\Component;

class Revisi extends Component
{
    public $i;
    public $nama;
    public $periode;
    public $revisi;
    public $foto;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = LPJ::where('id', $id)->exists();
        if ($test) {
            $this->i = $id;
            $lpj = LPJ::find($id);
            $this->nama = $lpj->proposal->nama;
            $this->periode = $lpj->periode->tahun;
            $this->foto = Foto::where('lpj_id', $id)->get();
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'revisi' => 'required'
        ]);
    }

    public function revisi()
    {
        $this->validate([
            'revisi' => 'required'

        ]);

        Revisi2::create([
            'lpj_id' => $this->i,
            'revisi' => $this->revisi,
        ]);

        LPJ::where('id', $this->i)->update([
            'status' => 'Revisi',
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $this->nama berhasil direvisi",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        return view('livewire.lpj.revisi')->extends('layouts.dpk', ['title' => 'Revisi LPJ'])->section('content');
    }
}
