<?php

namespace App\Http\Livewire\Lpj;

use App\Models\Detail;
use App\Models\LPJ;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $j;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        if (auth()->user()->level == 'BEM') {
            $this->j = Detail::where('bem_id', auth()->user()->bem->id)->orderBy('updated_at', 'desc')->first()->kementrian_id;
        }
    }

    public function hapus($id)
    {
        $proposal = LPJ::find($id);
        if (auth()->user()->level == 'DPK') {
            $this->showConfirmation($proposal->id, $proposal->proposal->nama);
        } else if (auth()->user()->level == 'BEM') {
            $this->showConfirmation2($proposal->id, $proposal->proposal->nama);
        }
    }

    public function hancur($id)
    {
        $nm = LPJ::find($id)->proposal->nama;
        if (auth()->user()->level == 'DPK') {
            LPJ::where('id', $id)->update([
                'status' => 'Disetujui',
            ]);
            $this->showModal($nm);
        } else if (auth()->user()->level == 'BEM') {
            LPJ::where('id', $id)->delete();
            $this->showModal2($nm);
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $nama berhasil disetujui",
        ]);
    }

    public function showModal2($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menyetujui LPJ $nama?",
            'text'        => "",
            'confirmText' => 'Ya, setuju!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin meghapus LPJ $nama?",
            'text'        => "",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }


    public function render()
    {
        $lpj = LPJ::selectRaw('lpj.*')->join('proposal', 'proposal.id', '=', 'lpj.proposal_id')->where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        if (auth()->user()->level == 'DPK') {
            return view('livewire.lpj.data', compact(['lpj']))->extends('layouts.dpk', ['title' => 'Data LPJ'])->section('content');
        } else if (auth()->user()->level == 'BEM') {
            return view('livewire.lpj.data', compact(['lpj']))->extends('layouts.bem', ['title' => 'Data LPJ'])->section('content');
        }
    }
}
