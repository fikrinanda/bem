<?php

namespace App\Http\Livewire\Lpj;

use App\Models\LPJ;
use App\Models\Proposal;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $i;
    public $proposal_id;
    public $nama;
    public $file;
    public $file2;
    public $foto = [];
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $this->i = $id;

        $test = LPJ::where('id', $id)->exists();
        if ($test) {
            $p = LPJ::find($id);
            $this->file = $p->file;
            $this->proposal_id = $p->proposal_id;
            $this->nama = Proposal::find($p->proposal_id)->nama;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'proposal_id' => 'required',
            // 'file' => 'required',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'proposal_id' => 'required',
        ]);

        if ($this->file2) {
            Storage::disk('public')->delete($this->file);
            $file2 = $this->file2->store('file/lpj', 'public');
        } else {
            $file2 = $this->file ?? null;
        }

        LPJ::where('id', $this->i)->update([
            'proposal_id' => $this->proposal_id,
            'file' => $file2
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        $proposal = Proposal::where('status', 'Disetujui')->get();
        return view('livewire.lpj.ubah', compact(['proposal']))->extends('layouts.bem', ['title' => 'Ubah LPJ'])->section('content');
    }
}
