<?php

namespace App\Http\Livewire\Lpj;

use App\Models\Detail;
use App\Models\DetailLPJ;
use App\Models\DetailProposal;
use App\Models\DPK;
use App\Models\Foto;
use App\Models\LPJ;
use App\Models\Proposal;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Tambah extends Component
{
    use WithFileUploads;

    public $proposal_id;
    public $file;
    public $nama;
    public $foto = [];
    protected $listeners = ['berhasil'];

    public function mount($proposal_id)
    {
        $test = Proposal::where('id', $proposal_id)->exists();
        $seul = DetailProposal::where('proposal_id', $proposal_id)->where('status', 'Setuju')->exists();
        $a = Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->kementrian_id;
        if ($a == null && $test == false) {
            abort('404');
        } else {
            if ($seul) {
                $info = Proposal::find($proposal_id);
                $this->nama = $info->nama . " Tahun " . $info->periode->tahun;
            } else {
                abort('404');
            }
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'file' => 'required',
            'foto.*' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'file' => 'required',
            'foto.*' => 'required|image|max:5000',
        ]);

        $file = $this->file->store('file/lpj', 'public');

        $x = LPJ::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "LP" . sprintf("%04s", $y);

        LPJ::create([
            'id' => $z,
            'bem_id' => auth()->user()->bem->id,
            'proposal_id' => $this->proposal_id,
            'periode_id' => Detail::where('bem_id', auth()->user()->bem->id)->orderBy('created_at', 'desc')->first()->periode_id,
            'dpk_id' => DPK::where('status', 'Utama')->first()->id,
        ]);

        $n = Proposal::find($this->proposal_id);
        $z1 = $n->periode->tahun;
        foreach ($this->foto as $photo) {
            $f = $photo->store("file/lpj/Foto $n->nama - $z1", 'public');
            Foto::create([
                'lpj_id' => $z,
                'foto' => $f
            ]);
        }

        DetailLPJ::create([
            'lpj_id' => $z,
            'hash' => Str::random(32),
            'file' => $file,
            'status' => "Menunggu Persetujuan",
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "LPJ $this->nama berhasil diajukan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/proposal/data');
    }

    public function render()
    {
        return view('livewire.lpj.tambah')->extends('layouts.bem', ['title' => 'Mengajukan LPJ'])->section('content');
    }
}
