<?php

namespace App\Http\Livewire\Dpk;

use App\Models\DPK;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];
    public $status;
    public $s;
    protected $rules = [
        'status.*' => 'required',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->status = DPK::get();

        foreach ($this->status as $key => $value) {
            $this->s[] = $value->status;
        }
    }

    public function updatedS()
    {
        foreach ($this->status as $key => $value) {
            DPK::where('id', $value->id)->update([
                'status' => $this->s[$key]
            ]);
        }
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->dpk->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->dpk->nama;
        $dpk = DPK::where('user_id', $id)->first();
        Storage::disk('public')->delete($dpk->foto);
        $user->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data DPK $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus DPK $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $dpk = DPK::selectRaw('dpk.*')->join('users', 'users.id', '=', 'dpk.user_id')->where('nama', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.dpk.data', compact(['dpk']))->extends('layouts.admin', ['title' => 'Data DPK'])->section('content');
    }
}
