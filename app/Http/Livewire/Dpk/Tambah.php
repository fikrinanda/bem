<?php

namespace App\Http\Livewire\Dpk;

use App\Models\DPK;
use App\Models\Prodi;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $prodi;
    public $foto;
    public $username;
    public $password;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/dpk/foto', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'DPK',
        ]);

        $user = User::where('username', $this->username)->first();

        $x2 = DPK::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "DK" . sprintf("%04s", $y2);

        DPK::create([
            'id' => $z2,
            'user_id' => $user->id,
            'nama' => $this->nama,
            'prodi_id' => $this->prodi,
            'foto' => $foto,
            'status' => 'Aktif'
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data DPK $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/dpk/data');
    }

    public function render()
    {
        $pd = Prodi::get();
        return view('livewire.dpk.tambah', compact(['pd']))->extends('layouts.admin', ['title' => 'Tambah DPK'])->section('content');
    }
}
