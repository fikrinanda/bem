<?php

namespace App\Http\Livewire\Dpk;

use App\Models\DPK;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $nama;
    public $prodi;
    public $foto;
    public $uname;
    public $password;
    public $i;

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $dpk = DPK::where('user_id', $user->id)->first();
            if ($dpk) {
                $this->i = $dpk->id;
                $this->nama = $dpk->nama;
                $this->prodi = $dpk->prodi->nama;
                $this->foto = $dpk->foto;
                $this->uname = $user->username;
                $this->password = $user->password;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.dpk.lihat')->extends('layouts.admin', ['title' => 'Lihat DPK'])->section('content');
    }
}
