<?php

namespace App\Http\Livewire\Dpk;

use App\Models\DPK;
use App\Models\Prodi;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $prodi;
    public $foto;
    public $foto2;
    public $i;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $dpk = DPK::where('user_id', $user->id)->first();
            if ($dpk) {
                $this->i = $dpk->id;
                $this->nama = $dpk->nama;
                $this->prodi = $dpk->prodi_id;
                $this->foto = $dpk->foto;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'prodi' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/dpk/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        DPK::where('id', $this->i)->update([
            'nama' => $this->nama,
            'prodi_id' => $this->prodi,
            'foto' => $foto2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data DPK $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/dpk/data');
    }

    public function render()
    {
        $pd = Prodi::get();
        return view('livewire.dpk.ubah', compact(['pd']))->extends('layouts.admin', ['title' => 'Ubah DPK'])->section('content');
    }
}
