<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logout extends Component
{
    public function mount()
    {
        auth()->logout();
        return redirect('/');
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.logout')->extends('layouts.admin')->section('content');
        } else if (auth()->user()->level == 'DPK') {
            return view('livewire.logout')->extends('layouts.dpk')->section('content');
        } else if (auth()->user()->level == 'BEM') {
            return view('livewire.logout')->extends('layouts.bem')->section('content');
        }
    }
}
