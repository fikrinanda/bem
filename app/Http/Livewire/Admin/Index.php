<?php

namespace App\Http\Livewire\Admin;

use App\Models\BEM;
use App\Models\Detail;
use App\Models\DetailLPJ;
use App\Models\Foto;
use App\Models\LPJ;
use App\Models\Periode;
use App\Models\Proposal;
use Livewire\Component;

class Index extends Component
{
    public $tahun;
    public $s;
    public $total;
    public $laksana;
    public $ang;
    public $search = '';

    public function mount()
    {
        $this->tahun = Periode::orderBy('id', 'desc')->first()->id;
        $this->s = Periode::find($this->tahun)->tahun;
        $this->total = Proposal::where('periode_id', $this->tahun)->count();
        $this->laksana = DetailLPJ::join('lpj', 'lpj.id', '=', 'detail_lpj.lpj_id')->where('periode_id', $this->tahun)->where('status', 'Setuju')->count();
        $this->ang = Detail::where('periode_id', $this->tahun)->count();
    }

    public function updatedTahun()
    {
        $this->s = Periode::find($this->tahun)->tahun;
        $this->total = Proposal::where('periode_id', $this->tahun)->count();
        $this->laksana = DetailLPJ::join('lpj', 'lpj.id', '=', 'detail_lpj.lpj_id')->where('periode_id', $this->tahun)->where('status', 'Setuju')->count();
        $this->ang = Detail::where('periode_id', $this->tahun)->count();
    }

    public function render()
    {
        $foto = null;
        $bem = Detail::select('*')->join('bem', 'bem.id', '=', 'detail_periode.bem_id')->where('bem.nama', 'like', '%' . $this->search . '%')->where('periode_id', $this->tahun)->get();
        $periode = Periode::orderBy('id', 'desc')->get();
        $lpj = DetailLPJ::join('lpj', 'lpj.id', '=', 'detail_lpj.lpj_id')->where('periode_id', $this->tahun)->where('status', 'Setuju')->get();
        foreach ($lpj as $key => $value) {
            $t = Foto::where('lpj_id', $value->id)->get();
            foreach ($t as $key2 => $value2) {
                $foto[$value->lpj_id][] = $value2->foto;
            }
        }
        return view('livewire.admin.index', compact(['bem', 'periode', 'lpj', 'foto']))->extends('layouts.admin', ['title' => 'Admin'])->section('content');
    }
}
