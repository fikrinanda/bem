<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login extends Component
{
    public $username;
    public $password;

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->first();
        if ($user && Hash::check($this->password, $user->password)) {
            Auth::login($user);
            if ($user->level == 'Admin') {
                return redirect()->intended('/admin');
            } else if ($user->level == 'DPK') {
                return redirect()->intended('/dpk');
            } else if ($user->level == 'BEM') {
                return redirect()->intended('/bem');
            } else {
                return back()->with('error', 'Username / Password Salah')->with($this->showModal());
            }
        } else {
            return back()->with('error', 'Username / Password Salah')->with($this->showModal());
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => "Username / Password Salah",
        ]);
    }

    public function render()
    {
        return view('livewire.login')->extends('layouts.login', ['title' => 'Login'])->section('content');
    }
}
