<?php

namespace App\Http\Livewire\Periode;

use App\Models\Periode;
use Livewire\Component;

class Ubah extends Component
{
    public $i;
    public $tahun;
    public $tahun2;
    public $saepol;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = Periode::where('id', $id);
        if ($test->exists()) {
            $this->i = $id;
            $thn = explode("/", $test->first()->tahun);
            $this->tahun = $thn[0];
            $this->tahun2 = $thn[1];
            $this->saepol = $test->first()->tahun;
        } else {
            abort('404');
        }
    }

    public function updatedTahun()
    {
        $this->tahun2 = $this->tahun + 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'tahun' => 'required|numeric|digits:4'
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'tahun' => 'required|numeric|digits:4'
        ]);

        $test = $this->tahun . '/' . ($this->tahun + 1);
        $watu = Periode::where('tahun', $test);
        if ($watu->exists()) {
            if ($watu->first()->id == $this->i) {
                Periode::where('id', $this->i)->update([
                    'tahun' => $this->tahun . '/' . ($this->tahun + 1)
                ]);

                $this->showModal();
            } else {
                $this->showAlert();
            }
        } else {
            Periode::where('id', $this->i)->update([
                'tahun' => $this->tahun . '/' . ($this->tahun + 1)
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Periode berhasil diubah",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => 'Tahun periode sudah ada!!',
            'timeout' => 3000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/periode/data');
    }

    public function render()
    {
        return view('livewire.periode.ubah')->extends('layouts.admin', ['title' => 'Ubah Periode'])->section('content');
    }
}
