<?php

namespace App\Http\Livewire\Periode;

use App\Models\Periode;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $periode = Periode::find($id);
        $this->showConfirmation($periode->id, $periode->tahun);
    }

    public function hancur($id)
    {
        $periode = Periode::find($id);
        $periode->delete();
        $this->showModal($periode->tahun);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Periode tahun $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Periode tahun $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $periode = Periode::where('tahun', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.periode.data', compact(['periode']))->extends('layouts.admin', ['title' => 'Data Periode'])->section('content');
    }
}
