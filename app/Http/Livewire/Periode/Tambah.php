<?php

namespace App\Http\Livewire\Periode;

use App\Models\Periode;
use Livewire\Component;

class Tambah extends Component
{
    public $tahun;
    public $tahun2;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'tahun' => 'required|numeric|digits:4',
        ]);
    }

    public function updatedTahun()
    {
        $this->tahun2 = $this->tahun + 1;
    }

    public function tambah()
    {
        $this->validate([
            'tahun' => 'required|numeric|digits:4',
        ]);

        $test = $this->tahun . '/' . ($this->tahun + 1);
        if (Periode::where('tahun', $test)->exists()) {
            $this->showAlert();
        } else {
            $x = Periode::max('id');
            $y = (int) substr($x, 2, 4);
            $y++;
            $z = "PR" . sprintf("%04s", $y);

            Periode::create([
                'id' => $z,
                'tahun' => $this->tahun . '/' . ($this->tahun + 1)
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Periode tahun $this->tahun" . '/' . ($this->tahun + 1) . " berhasil ditambahkan",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => 'Tahun periode sudah ada!!',
            'timeout' => 3000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/periode/data');
    }

    public function render()
    {
        return view('livewire.periode.tambah')->extends('layouts.admin', ['title' => 'Tambah Periode'])->section('content');
    }
}
