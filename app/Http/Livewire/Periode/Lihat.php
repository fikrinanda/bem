<?php

namespace App\Http\Livewire\Periode;

use App\Models\Detail;
use App\Models\Periode;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $i;
    public $search = '';
    public $perPage = 5;
    public $per;
    protected $paginationTheme = 'bootstrap';

    public function mount($id)
    {
        $test = Periode::where('id', $id);
        if ($test->exists()) {
            $this->i = $test->first()->tahun;
            $this->per = $id;
        } else {
            abort('404');
        }
    }

    public function render()
    {
        $bem = Detail::selectRaw('detail_periode.*')->join('bem', 'bem.id', '=', 'detail_periode.bem_id')->where('nama', 'like', '%' . $this->search . '%')->where('periode_id', $this->per)->paginate($this->perPage);
        return view('livewire.periode.lihat', compact(['bem']))->extends('layouts.admin', ['title' => 'Data BEM'])->section('content');
    }
}
