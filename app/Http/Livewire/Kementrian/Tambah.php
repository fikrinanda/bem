<?php

namespace App\Http\Livewire\Kementrian;

use App\Models\Kementrian;
use Livewire\Component;

class Tambah extends Component
{
    public $nama;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:kementrian,nama',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:kementrian,nama',
        ]);

        $x = Kementrian::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "KM" . sprintf("%04s", $y);

        Kementrian::create([
            'id' => $z,
            'nama' => $this->nama,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kementrian $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kementrian/data');
    }

    public function render()
    {
        return view('livewire.kementrian.tambah')->extends('layouts.admin', ['title' => 'Tambah Kementrian'])->section('content');
    }
}
