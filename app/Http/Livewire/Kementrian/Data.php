<?php

namespace App\Http\Livewire\Kementrian;

use App\Models\Kementrian;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $ktm = Kementrian::find($id);
        $this->showConfirmation($ktm->id, $ktm->nama);
    }

    public function hancur($id)
    {
        $ktm = Kementrian::find($id);
        $nama = $ktm->nama;
        $ktm->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kementrian $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Kementrian $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $kementrian = Kementrian::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.kementrian.data', compact(['kementrian']))->extends('layouts.admin', ['title' => 'Data Kementrian'])->section('content');
    }
}
