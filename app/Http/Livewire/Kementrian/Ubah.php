<?php

namespace App\Http\Livewire\Kementrian;

use App\Models\Kementrian;
use Livewire\Component;

class Ubah extends Component
{
    public $i;
    public $nama;
    public $saepol;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = Kementrian::where('id', $id);
        if ($test->exists()) {
            $this->i = $id;
            $this->nama = $test->first()->nama;
            $this->saepol = $test->first()->nama;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:kementrian,nama,' . $this->i,
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3|unique:kementrian,nama,' . $this->i,
        ]);

        Kementrian::where('id', $this->i)->update([
            'nama' => $this->nama,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kementrian $this->saepol berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kementrian/data');
    }

    public function render()
    {
        return view('livewire.kementrian.ubah')->extends('layouts.admin', ['title' => 'Ubah Kementrian' . $this->saepol])->section('content');
    }
}
