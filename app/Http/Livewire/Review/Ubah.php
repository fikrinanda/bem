<?php

namespace App\Http\Livewire\Review;

use App\Models\DetailLPJ;
use App\Models\LPJ;
use App\Models\Review;
use Livewire\Component;

class Ubah extends Component
{
    public $lpj_id;
    public $nama;
    public $rating;
    public $review;
    protected $listeners = ['berhasil', 'rt'];

    public function mount($lpj_id)
    {
        $test = LPJ::where('id', $lpj_id)->exists();
        $test2 = DetailLPJ::where('lpj_id', $lpj_id)->where('status', 'Setuju')->exists();
        $test3 = Review::where('dpk_id', auth()->user()->dpk->id)->exists();
        if ($test && $test2 && $test3) {
            $n = LPJ::find($lpj_id);
            $this->nama = $n->proposal->nama . " Tahun " . $n->periode->tahun;
            $krupuk = Review::where('dpk_id', auth()->user()->dpk->id)->where('lpj_id', $lpj_id)->first();
            $this->rating = $krupuk->rating;
            $this->review = $krupuk->review;
        } else {
            abort('404');
        }
    }

    public function updated($fiels)
    {
        $this->validateOnly($fiels, [
            'rating' => 'required',
            'review' => 'required'
        ]);
    }

    public function rt($value)
    {
        if (!is_null($value)) {
            $this->rating = $value;
        }
    }

    public function ubah()
    {
        $this->validate([
            'rating' => 'required',
            'review' => 'required'
        ]);

        Review::where('dpk_id', auth()->user()->dpk->id)->where('lpj_id', $this->lpj_id)->update([
            'rating' => $this->rating,
            'review' => $this->review
        ]);

        $this->showModal("Berhasil mengubah review");
    }

    public function showModal($text)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("review/data/$this->lpj_id");
    }

    public function render()
    {
        return view('livewire.review.ubah')->extends('layouts.dpk', ['title' => 'Ubah Review'])->section('content');
    }
}
