<?php

namespace App\Http\Livewire\Review;

use App\Models\DetailLPJ;
use App\Models\LPJ;
use App\Models\Review;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $lpj_id;
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $pit;
    public $iki;

    public function mount($lpj_id)
    {
        $test = LPJ::where('id', $lpj_id)->exists();
        $test2 = DetailLPJ::where('lpj_id', $lpj_id)->where('status', 'Setuju')->exists();

        if ($test && $test2) {
            $n = LPJ::find($lpj_id);
            $this->pit = $n->proposal->nama . " Tahun " . $n->periode->tahun;
            if (auth()->user()->level == "DPK") {
                $this->iki = Review::where('dpk_id', auth()->user()->dpk->id)->where('lpj_id', $lpj_id)->exists();
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        $review = Review::where('lpj_id', $this->lpj_id)->paginate($this->perPage);
        if (auth()->user()->level == "DPK") {
            return view('livewire.review.data', compact(['review']))->extends('layouts.dpk', ['title' => 'Review'])->section('content');
        } else if (auth()->user()->level == "BEM") {
            return view('livewire.review.data', compact(['review']))->extends('layouts.bem', ['title' => 'Review'])->section('content');
        }
    }
}
