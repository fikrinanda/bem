<?php

namespace App\Http\Livewire\Review;

use App\Models\DetailLPJ;
use App\Models\LPJ;
use App\Models\Review;
use Livewire\Component;

class Tambah extends Component
{
    public $lpj_id;
    public $nama;
    public $rating;
    public $review;
    protected $listeners = ['berhasil', 'rt'];

    public function mount($lpj_id)
    {
        $test = LPJ::where('id', $lpj_id)->exists();
        $test2 = DetailLPJ::where('lpj_id', $lpj_id)->where('status', 'Setuju')->exists();
        $test3 = Review::where('dpk_id', auth()->user()->dpk->id)->exists();
        if ($test && $test2) {
            $n = LPJ::find($lpj_id);
            $this->nama = $n->proposal->nama . " Tahun " . $n->periode->tahun;
        } else {
            if ($test3) {
                # code...
            } else {
                abort('404');
            }
        }
    }

    public function updated($fiels)
    {
        $this->validateOnly($fiels, [
            'rating' => 'required',
            'review' => 'required'
        ]);
    }

    public function rt($value)
    {
        if (!is_null($value)) {
            $this->rating = $value;
        }
    }

    public function tambah()
    {
        $this->validate([
            'rating' => 'required',
            'review' => 'required'
        ]);

        Review::create([
            'dpk_id' => auth()->user()->dpk->id,
            'lpj_id' => $this->lpj_id,
            'rating' => $this->rating,
            'review' => $this->review
        ]);

        $this->showModal("Berhasil menambah review");
    }

    public function showModal($text)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("review/data/$this->lpj_id");
    }

    public function render()
    {
        return view('livewire.review.tambah')->extends('layouts.dpk', ['title' => 'Tambah Review'])->section('content');
    }
}
