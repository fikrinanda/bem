<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $table = 'review';
    protected $fillable = ['dpk_id', 'lpj_id', 'rating', 'review'];
    public $incrementing = false;

    public function dpk()
    {
        return $this->hasOne(DPK::class, 'id', 'dpk_id');
    }

    public function lpj()
    {
        return $this->hasOne(LPJ::class, 'id', 'lpj_id');
    }
}
