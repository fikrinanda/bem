<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revisi extends Model
{
    use HasFactory;

    protected $table = 'revisi_proposal';
    protected $fillable = ['proposal_id', 'revisi'];

    public function proposal()
    {
        return $this->hasOne(Proposal::class, 'id', 'proposal_id');
    }
}
