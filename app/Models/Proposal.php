<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    use HasFactory;

    protected $table = 'proposal';
    protected $fillable = ['id', 'bem_id', 'periode_id', 'dpk_id', 'nama'];
    public $incrementing = false;

    public function bem()
    {
        return $this->hasOne(BEM::class, 'id', 'bem_id');
    }

    public function periode()
    {
        return $this->hasOne(Periode::class, 'id', 'periode_id');
    }

    public function dpk()
    {
        return $this->hasOne(DPK::class, 'id', 'dpk_id');
    }
}
