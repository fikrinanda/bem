<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DPK extends Model
{
    use HasFactory;

    protected $table = 'dpk';
    protected $fillable = ['id', 'user_id', 'nama', 'prodi_id', 'foto', 'dpk'];
    public $incrementing = false;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }
}
