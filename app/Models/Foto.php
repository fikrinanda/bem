<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    use HasFactory;

    protected $primaryKey = 'lpj_id';
    protected $table = 'foto_kegiatan';
    protected $fillable = ['lpj_id', 'foto'];
    public $incrementing = false;

    public function lpj()
    {
        return $this->hasOne(LPJ::class, 'id', 'lpj_id');
    }
}
