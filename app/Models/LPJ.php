<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LPJ extends Model
{
    use HasFactory;

    protected $table = 'lpj';
    protected $fillable = ['id', 'bem_id', 'periode_id', 'proposal_id', 'dpk_id'];
    public $incrementing = false;

    public function bem()
    {
        return $this->hasOne(BEM::class, 'id', 'bem_id');
    }

    public function proposal()
    {
        return $this->hasOne(Proposal::class, 'id', 'proposal_id');
    }

    public function periode()
    {
        return $this->hasOne(Periode::class, 'id', 'periode_id');
    }

    public function dpk()
    {
        return $this->hasOne(DPK::class, 'id', 'dpk_id');
    }
}
