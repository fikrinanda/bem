<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailProposal extends Model
{
    use HasFactory;

    protected $primaryKey = 'hash';
    protected $table = 'detail_proposal';
    protected $fillable = ['proposal_id', 'hash', 'file', 'status', 'revisi'];
    public $incrementing = false;

    public function proposal()
    {
        return $this->hasOne(Proposal::class, 'id', 'proposal_id');
    }
}
