<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailLPJ extends Model
{
    use HasFactory;

    protected $primaryKey = 'hash';
    protected $table = 'detail_lpj';
    protected $fillable = ['lpj_id', 'hash', 'file', 'status', 'revisi'];
    public $incrementing = false;

    public function lpj()
    {
        return $this->hasOne(LPJ::class, 'id', 'lpj_id');
    }
}
