<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    protected $table = 'detail_periode';
    protected $fillable = ['bem_id', 'kementrian_id', 'jabatan', 'periode_id', 'hash'];
    public $incrementing = false;
    public $primaryKey = 'hash';

    public function bem()
    {
        return $this->hasOne(BEM::class, 'id', 'bem_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function kementrian()
    {
        return $this->hasOne(Kementrian::class, 'id', 'kementrian_id');
    }

    public function periode()
    {
        return $this->hasOne(Periode::class, 'id', 'periode_id');
    }
}
