<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revisi2 extends Model
{
    use HasFactory;

    protected $table = 'revisi_lpj';
    protected $fillable = ['lpj_id', 'revisi'];

    public function lpj()
    {
        return $this->hasOne(LPJ::class, 'id', 'lpj_id');
    }
}
