<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BEM extends Model
{
    use HasFactory;

    protected $table = 'bem';
    protected $fillable = ['id', 'user_id', 'nama', 'prodi_id', 'foto'];
    public $incrementing = false;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function kementrian()
    {
        return $this->hasOne(Kementrian::class, 'id', 'kementrian_id');
    }

    public function detail()
    {
        return $this->hasOne(Detail::class, 'bem_id', 'id');
    }
}
